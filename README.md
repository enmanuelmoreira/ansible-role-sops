# Ansible Role: Sops

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-sops/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-sops/-/commits/main)

This role installs [Sops](https://github.com/mozilla/sops) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    sops_version: latest # v3.7.0 if you want a specific version
    sops_package_name: sops
    setup_dir: /tmp
    sops_bin_path: /usr/local/bin/sops
    sops_repo_path: https://github.com/mozilla/sops/releases/download

This role can install the latest or a specific version. See [available Sops releases](https://github.com/mozilla/sops/releases/) and change this variable accordingly.

    sops_version: latest # v3.7.0 if you want a specific version

The path of the Sops repository.

    sops_repo_path: https://github.com/mozilla/sops/releases/download

The path to the home Sops directory.

    sops_bin_path: /usr/local/bin/sops

The location where the Sops binary will be installed.

Sops needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Sops. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: sops

## License

MIT / BSD
